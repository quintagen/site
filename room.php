<?php
	session_start();

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="16x16" href="res/images/SamLogo.png">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Goudy+Bookletter+1911" />
    <link href="https://fonts.googleapis.com/css?family=Wendy+One" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="res/css/room.min.css?v1.1">
    <title>Room</title>
</head>
<body>
    <div class="container">
    	<div class="blocker">
			<span class="helper"></span><img src="res/images/screen_rotation.png" class="rotate" />
			<span class="rotate_reminder">Please rotate your screen</span>
		</div>
    	<div class="loading">
			<img class="loading-logo" src="res/images/SamLogo.svg">
			<span class="loading-title">LOADING</span>
			<div class="dots">
				<span class="dot0">.</span>
				<span class="dot1">.</span>
				<span class="dot2">.</span>
				<span class="dot3">.</span>
				<span class="dot4">.</span>
				<span class="dot5">.</span>
			</div> 
		</div>
        <div class="header">
            <img src="res/images/SamLogo.svg">
            <i id="toggleVolume" class="fas fa-volume-up"></i>
        </div>
        <div class="html5-progress-bar">
			<div class="progress-bar-wrapper">
				<progress class="progressbar"></progress>
				<span class="progress-value"></span>
			</div>
		</div>
        <div class="content">
        	<div class="circle a green"></div>
			<div class="circle b green"></div>
			<div class="circle c green"></div>
			<div class="circle d green"></div>
			<div class="circle e yellow"></div>
			<div class="circle f yellow"></div>
			<div class="circle g yellow"></div>
			<div class="circle h yellow"></div>
			<div class="circle i green"></div>
			<div class="circle j yellow"></div>
        	<div class="ques-ans">
	            <h2>Sam Mental Arithmetic</h2>
	            <span class="number word"></span>
	            <span class="answer-title">Answer</span>
	            <input type="number" class="answer"/>
	        </div>
	        <div class="score">
	        	<div class="congrats">
					<h1>Congratulations!</h1>
				</div>
	        	<h2 class="score_title">SCORE</h2>
	        	<div class="percentage">
		        	<span class="real_score">80</span><!--
		        	--><span class="percent_sign">%</span>
		        </div>
	        	<span class="scale">4/5</span>
	        	<button class="playAgain">Play Again</button>
	        	<div class="result">
	        		<span class="answer-box correct">
	        			<i class="fas fa-check"></i>
	        			<span class="question-number">Question 1</span>
	        			<span class="right-answer">35</span>
	        		</span>
	        		<span class="answer-box wrong">
	        			<i class="fas fa-times"></i>
	        			<span class="question-number">Question 2</span>
	        			<span class="right-answer">27</span>
	        		</span>
	        		<span class="answer-box correct">
	        			<i class="fas fa-check"></i>
	        			<span class="question-number">Question 3</span>
	        			<span class="right-answer">54</span>
	        		</span>
	        		<span class="answer-box wrong">
	        			<i class="fas fa-times"></i>
	        			<span class="question-number">Question 4</span>
	        			<span class="right-answer">62</span>
	        		</span>
	        		<span class="answer-box correct">
	        			<i class="fas fa-check"></i>
	        			<span class="question-number">Question 5</span>
	        			<span class="right-answer">39</span>
	        		</span>
	        		<span class="answer-box correct">
	        			<i class="fas fa-check"></i>
	        			<span class="question-number">Question 5</span>
	        			<span class="right-answer">39</span>
	        		</span>
	        		<span class="answer-box correct">
	        			<i class="fas fa-check"></i>
	        			<span class="question-number">Question 5</span>
	        			<span class="right-answer">39</span>
	        		</span>
	        		<span class="answer-box correct">
	        			<i class="fas fa-check"></i>
	        			<span class="question-number">Question 5</span>
	        			<span class="right-answer">39</span>
	        		</span>
	        		<span class="answer-box correct">
	        			<i class="fas fa-check"></i>
	        			<span class="question-number">Question 5</span>
	        			<span class="right-answer">39</span>
	        		</span>
	        		<span class="answer-box correct">
	        			<i class="fas fa-check"></i>
	        			<span class="question-number">Question 5</span>
	        			<span class="right-answer">39</span>
	        		</span>

	        	</div>
	        </div>
        </div>        
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.4/gsap.min.js"></script>
    <script type="text/javascript" src="res/js/underscore-min.js"></script>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.quintagen.com/res/js/tools_v1.0.min.js"></script >
    <script type="text/javascript" src="res/js/default.min.js?v1.0"></script>
    <script type="text/javascript" src="res/js/loading.min.js"></script>
    <script type="text/javascript" src="algo/algo.min.js?v1.2"></script>
    <script type="text/javascript" src="res/js/room.min.js?v1.3"></script>

</body>
</html>

