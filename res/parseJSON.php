<?php

session_start();

if(isset($_SESSION["no_of_question"]) == false){
	echo json_encode(['status'=>false]);
}else{
	echo json_encode([
		'status'=>true,
		"nQues" => $_SESSION["no_of_question"],
		"nRow" => $_SESSION["no_of_row"],
		"nDig" => $_SESSION["no_of_digit"],
		"timeInt" => $_SESSION["time_interval"],
		"ansTime" => $_SESSION["answering_time"],
		"timer" => $_SESSION["timer"],
		"qType" => $_SESSION["question_type"],
		"delay" => $_SESSION["delay"],
		"level" => $_SESSION["level"]
	]);
}


?>