var colors = ["midnightblue", "aliceblue","saddlebrown", "blanchedalmond", "black", "white"];

var arr = [];
var ans = [];
var numOfQuestion = 3;
var numOfRow = 5;
var numOfDigit = 2;
var operator = 2;
var timeInterval;
var answerTime;
var level;
var R1;
var timeup = false;
var out;
var interval_;
var animate;
var totalTime = 150; //using seconds not ms
var startTime = 150; //using seconds not ms
var delay = 0; //using ms

$.ajax({
    type: "POST",
    url: "res/parseJSON.php",
    dataType: "json",
    data: {
    	status: true
    },
    success : function(data){
    	if(data.status == false){
    		location.replace("chooseLevel.php");
    	}else{
    		numOfQuestion = parseInt(data.nQues);
			numOfRow = parseInt(data.nRow);
			numOfDigit = data.nDig;
			timeInterval = parseFloat(data.timeInt);
			answerTime = parseInt(parseInt(data.ansTime) / 10);
			totalTime = parseFloat(data.timer);
			startTime = parseFloat(data.timer);
			operator = parseInt(data.qType);
			delay = parseInt(data.delay);
			level = parseInt(data.level);

			R1 = new Room(numOfQuestion,numOfRow,numOfDigit,operator,level);
			arr = R1.getQuestionArray();

			l(arr);

			if(numOfDigit >= 4){
				$(".number")[0].style.fontSize = "80px";
			}

			if(numOfDigit.toString().includes("1,")){
				$(".number")[0].style.fontSize = "70px";
			}

			if(numOfDigit.toString().includes("1.5,") || numOfDigit.toString().includes("2,")){
				$(".number")[0].style.fontSize = "60px";
			}

			$(".progressbar")[0].max = totalTime;
			$(".progressbar")[0].value = startTime;
			$(".progress-value")[0].innerHTML = totalTime.toFixed(2);

			//l(operator);
			//l(numOfRow);
			//l(numOfDigit);
			//l(data);
			//l(arr);

			$(".loading").fadeOut("fast");
	        $(".header").fadeIn("fast");
	        $(".html5-progress-bar")[0].style.display = "none";
	        //$(".html5-progress-bar").fadeIn("fast");
	        /*$(".content")[0].style.display = "none";
	        $(".congrats").fadeIn("slow");
	        animateCongrats();*/
	        $(".content").fadeIn("fast");

	        startCountDown().then(() => {
				timer(totalTime, startTime); //using seconds not ms
				//$(".html5-progress-bar")[0].style.display = "none";
				//$(".html5-progress-bar")[0].style.visibility = "hidden";
				start();
			});
			/*setTimeout(()=>{
				$(".loading").fadeOut("fast");

				//starto
				startCountDown().then(() => {
					timer(totalTime, startTime); //using seconds not ms
					$(".html5-progress-bar")[0].style.display = "none";
					start();
				});
			},2000)*/
			
    	}
    },
    error: function(error){
    	l(error);
    }
});

function animateCongrats() {
	var numberOfStars = 30;

	for (var i = 0; i < numberOfStars; i++) {
		$(".congrats").append('<div class="blob fa fa-star ' + i + '"></div>');
	}

	animateText();

	animateBlobs();
};

$('.congrats').click(function() {
	
});

function reset() {
	$.each($(".blob"), function(i) {
		TweenMax.set($(this), { x: 0, y: 0, opacity: 1 });
	});

	TweenMax.set($("h1"), { scale: 1, opacity: 1, rotation: 0 });
};

function animateText() {
	TweenMax.from($("h1"), 0.8, {
		scale: 0.4,
		opacity: 0,
		rotation: 15,
		ease: Back.easeOut.config(4)
	});
};

function animateBlobs() {
	var xSeed = _.random(350, 380);
	var ySeed = _.random(120, 170);

	$.each($(".blob"), function(i) {
		var $blob = $(this);
		var speed = _.random(1, 5);
		var rotation = _.random(5, 100);
		var scale = _.random(0.8, 1.5);
		var x = _.random(-xSeed, xSeed);
		var y = _.random(-ySeed, ySeed);

		TweenMax.to($blob, speed, {
			x: x,
			y: y,
			ease: Power1.easeOut,
			opacity: 0,
			rotation: rotation,
			scale: scale,
			onStartParams: [$blob],
			onStart: function($element) {
				$element.css("display", "block");
			},
			onCompleteParams: [$blob],
			onComplete: function($element) {
				$element.css("display", "none");
			}
		});
	});
};


/*function openChest() {
	$(".treasure")[0].style.display = "none";
	$(".treasure_gif")[0].style.display = "block";

	setTimeout(()=> {
		//$(".surprise_circle").fadeOut("slow");
		$(".surprise_box").fadeIn("slow");
	}, 1500);
})*/

window.onbeforeunload = function (e){
	$.ajax({
	    type: "POST",
	    url: "res/closeRoom.php",
	    dataType: "json",
	    data: {},
	    success : function(data){
	    	//l(data);
	    },
	    error: function(error){
	    	l(error);
	    }
	});
};

function timer(totalTime, startTime) {
	$(".progressbar")[0].max = totalTime;
	$(".progressbar")[0].value = startTime;

	var max = $(".progressbar")[0].max;
	var value = $(".progressbar")[0].value;
    $(".progress-value")[0].innerHTML = value;

    var loading = function() {
        value -= (1/100);
        $(".progressbar")[0].value = (value).toFixed(2);
        $(".progress-value")[0].innerHTML = (value).toFixed(2);

        if (value <= 0) {
        	timeup = true;
        	$(".progress-value")[0].innerHTML = "0.00";
    		if($(".answer")[0].value == "")
				ans.push(-1);
			else
				ans.push(parseInt($(".answer")[0].value));
            clearInterval(animate);
        }

        if(value > 30 && value <= 60)
	    	$(".progressbar").addClass("orange");

	    if(value <= 30)
	    {
	    	if(value <= 20 && value > 10)
		    {
		    	if(Math.floor((value * 10) % 10) == 0)
		    	{
		    		$(".progressbar").removeClass("red");
		    		$(".progressbar").addClass("orange");
		    	}
		    		
		    	else
		    	{
		    		$(".progressbar").removeClass("orange");
		    		$(".progressbar").addClass("red");
		    	}
		    }
		    else if(value <= 10)
		    {
		    	if(Math.floor((value * 10) % 5) == 0)
		    	{
		    		$(".progressbar").removeClass("red");
		    		$(".progressbar").addClass("orange");
		    	}
		    		
		    	else
		    	{
		    		$(".progressbar").removeClass("orange");
		    		$(".progressbar").addClass("red");
		    	}
		    }
		    else
		    {
		    	$(".progressbar").removeClass("orange");
				$(".progressbar").addClass("red");
		    }
	    }
    };    

    animate = setInterval(function() {
        loading();
    }, 10);
};

function stop() {
	var remainTime = ($(".progressbar")[0].value).toFixed(2);
	clearInterval(animate);
	//l(remainTime);
};

async function asyncForEach(array, callback) {
	for (let index = 0; index < array.length; index++) {
    	await callback(array[index], index, array);
  	}
};

function pushAnswer() {
	//l("HAHA");
	return new Promise((resolve,reject) => {
		if(!timeup){
			if($(".answer")[0].value == "")
				ans.push(-1);
			else
				ans.push($(".answer")[0].value);
		}
		$(".answer")[0].value = "";

		if(currentQuestion != arr.length)
		{
			setTimeout(()=> {
				//l("kaodim");
				$(".number")[0].style.display = "block";
				resolve();	
			},delay);
		}
		else
			resolve();
	})
	
}

var row = 0;
var column = 0;
var currentQuestion = 0;
const start = async () => {
	await asyncForEach(arr, async (ele) => {
		currentQuestion++;
		//l(currentQuestion + " : " + arr.length);
		await enterAnswer(ele);
		await pushAnswer();
  	});

	l(ans);
  	checkAnswer(ans);
};

var commentGood = ["Well Done! 😎", "Congratulations! 👨‍🎓👩‍🎓", "Superb! 😱"];
var commentBad = ["Keep It Up! 💪", "Add Oil! 💪", "You Can Be Better! 💪"];

function showResult(correctCount, totalQuestion) {
	$(".scale")[0].innerHTML = correctCount + "/" + totalQuestion;
	$(".real_score")[0].innerHTML = parseInt(correctCount/totalQuestion*100);

	if(parseInt(correctCount/totalQuestion*100) == 100){
		$(".score_title")[0].innerHTML = commentGood[Math.floor(Math.random() * 3)];
		$(".content")[0].style.display = "none";
		$(".header")[0].style.display = "none";
		//$(".html5-progress-bar")[0].style.display = "none";
		$(".congrats").fadeIn("slow");
		animateCongrats();
		//$(".score_title").fadeOut(100);
		//$(".scale").fadeOut(100);
		//$(".real_score").fadeOut(100);
		//$(".percent_sign").fadeOut(100);

		$(".score")[0].style.display = "block";
		$(".score_title")[0].style.visibility = "hidden";
		$(".scale")[0].style.visibility = "hidden";
		$(".real_score")[0].style.visibility = "hidden";
		$(".percent_sign")[0].style.visibility = "hidden";
		$(".header")[0].style.display = "block";
		$(".html5-progress-bar")[0].style.display = "block";
	    //$(".html5-progress-bar")[0].style.visibility = "visible";
		$(".content").fadeIn("slow");

		setTimeout(()=> {
			$(".congrats").fadeOut("fast");
			//$(".score_title").fadeIn("slow");
			//$(".scale").fadeIn("slow");
			//$(".real_score").fadeIn("slow");
			//$(".percent_sign").fadeIn("slow");
			//$(".score_title").addClass("visi");
			//$(".scale").addClass("visi");
			//$(".real_score").addClass("visi");
			//$(".percent_sign").addClass("visi");

			$(".score_title")[0].style.visibility = "visible";
			$(".scale")[0].style.visibility = "visible";
			$(".real_score")[0].style.visibility = "visible";
			$(".percent_sign")[0].style.visibility = "visible";

			/*$(".score")[0].style.display = "block";
			$(".congrats").fadeOut("fast");
			$(".header").fadeIn("slow");
			$(".html5-progress-bar").fadeIn("slow");
		    //$(".html5-progress-bar")[0].style.visibility = "visible";
			$(".content").fadeIn("slow");*/

		},2500);
	}
	else {
		if(parseInt(correctCount/totalQuestion*100) < 70)
			$(".score_title")[0].innerHTML = commentBad[Math.floor(Math.random() * 3)];

		$(".score")[0].style.display = "block";
		$(".header")[0].style.display = "block";
		$(".html5-progress-bar")[0].style.display = "block";
	    //$(".html5-progress-bar")[0].style.visibility = "visible";
	    $(".content").fadeIn("slow");
	}
	
	

	$.ajax({
	    type: "POST",
	    url: "res/closeRoom.php",
	    dataType: "json",
	    data: {},
	    success : function(data){
	    	//l(data);
	    },
	    error: function(error){
	    	l(error);
	    }
	});

	
};

$(".playAgain")[0].addEventListener("click",() => {
	l("HAHA");
	$(".loading")[0].style.display = "block";
    $(".header")[0].style.display = "none"; 
    $(".html5-progress-bar")[0].style.display = "none";
    $(".content")[0].style.display = "none"; 
    $(".congrats")[0].style.display = "none";
	arr = [];
	ans = [];
	numOfQuestion = 0;
	numOfRow = 0;
	numOfDigit = 0;
	operator = 0;
	R1 = "";
	arr = "";
	
	timeup = false;
	out = "";
	interval_ = "";
	animate = "";
	totalTime = 0; //using seconds not ms
	startTime = 0; //using seconds not ms

	$(".ques-ans")[0].style.display = "block";
	$(".score")[0].style.display = "none";
	$(".ques-ans h2")[0].innerHTML = "Sam Mental Arithmetic";
	$(".number")[0].style.display = "block";
	$(".progressbar")[0].max = 1;
	$(".progressbar")[0].value = 0;
	$(".progress-value")[0].innerHTML = 0;
	$(".progressbar").removeClass("red");
	$(".progressbar").removeClass("orange");
	location.href = "chooseLevel.php?p";
});

async function checkAnswer (ans)
{
	stop();
	var correctCount = 0;
	$(".ques-ans")[0].style.display = "none";
	$(".result")[0].innerHTML = "";
	$(".loading")[0].style.display = "block";

	await new Promise((resolve,reject) => {
		var counter = 0;
		ans.forEach(ele => {
			//l(R1.answer(counter));
			if(ele == R1.answer(counter))
			{
				correctCount++;
				$(".result")[0].innerHTML += '<span class="answer-box correct"><i class="fas fa-check"></i><span class="question-number">Question ' + (counter+1) + '</span><span class="right-answer">' + R1.answer(counter) + '</span></span>';
				//l(true);
			}
			else
			{
				$(".result")[0].innerHTML += '<span class="answer-box wrong"><i class="fas fa-times"></i><span class="question-number">Question ' + (counter+1) + '</span><span class="right-answer">' + R1.answer(counter) + '</span></span>';
				//l(false);
			}

			counter++;
		});


		if(counter == R1.question.length)
			resolve();
	}).then(() => {
		setTimeout(()=> {
			$(".loading").fadeOut("fast");
			showResult(correctCount,R1.question.length)
		},2000);		
	});
	
};

function startCountDown() {
	var n = 2;
	var fadeTime = 750; //ms
	return new Promise((resolve,reject) => {
		var tm = setInterval(()=> {
			if(n == 0){
				clearInterval(tm);
				//console.log(n);
				resolve();
			}
			else
			{
				//console.log(n);
			    
			    if(n == 2)
			    {
			    	$(".number")[0].innerHTML = "Ready";
					$(".number").fadeOut(0).fadeIn(fadeTime).fadeOut(fadeTime);
			    }
			    else
			    {
			    	$(".number")[0].innerHTML = "Go";
			    	$(".number").fadeIn(fadeTime).fadeOut(fadeTime,()=>
			    		{
			    			$(".number").removeClass("word");
			    			$(".number")[0].style.display = "block";
			    		});
			    }
			}
			n--;

		},1500);
	})
	
};

function enterAnswer(ele) {
	return new Promise((resolve, reject) => {
		if(timeup)
		{
			resolve();
			ans.push(-1);
			//l(ans);
		}
		else
		{
			showNumbers(ele).then(()=> {
				//console.log("Start");
				$(".number")[0].style.display = "none";
				$(".answer-title")[0].style.display = "block";
				$(".answer")[0].style.display = "block";
				$(".answer").focus();
				
				row++;
				//l(row);
				var abcde = 0;
				out = setInterval(()=>{
					if(timeup){
						//console.log("Timeup");
						//$(".number")[0].style.display = "block";
						$(".answer-title")[0].style.display = "none";
						$(".answer")[0].style.display = "none";
						column = 0;
						//alert("JAA");
						clearInterval(out);
						resolve();
					}else{
						if(abcde >= answerTime){
							//console.log("Finish. Next question");
							//$(".number")[0].style.display = "block";
							$(".answer-title")[0].style.display = "none";
							$(".answer")[0].style.display = "none";
							column = 0;
							clearInterval(out);
							/*setTimeout(()=>{
								l("resolved");
								
								resolve();
							},1000);*/
							resolve();
							
						}else{
							abcde++;
						}
					}
				},10);

				$(".answer")[0].addEventListener("change", ()=> {
					//console.log("Gaodim. Next question");
					//$(".number")[0].style.display = "block";
					$(".answer-title")[0].style.display = "none";
					$(".answer")[0].style.display = "none";
					column = 0;
					clearInterval(out);
					resolve();
				})

				
			});
		}		
	})	
};

function showNumbers(ele) {
	var error = false;
	$(".ques-ans h2")[0].innerHTML = "Question " + (row+1);
	$(".number")[0].style.color = colors[column%6];
	$(".number")[0].innerHTML = arr[row][0];
	//l("dark");
	column++;

	return new Promise((resolve, reject) => {
		interval_ = setInterval(function () {
			if(timeup)
			{
				resolve();
			}
			else
			{
				if(column >= ele.length)
				{
					//console.log("done");
					clearInterval(interval_);
					if(!error)
						resolve();
					else
						reject("Error: Something went wrong");
				}
				else
				{
					$(".number")[0].innerHTML = ele[column];

					if(column % 2 == 0)
					{
						//l(column%7);
						$(".number")[0].style.color = colors[column%6];
					}	
					else
					{
						//l(column%7);
						$(".number")[0].style.color = colors[column%6];
					}

					column++;
				}
			}
				
			
		}, timeInterval);
	});
};