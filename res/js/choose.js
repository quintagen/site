var no_of_question = 0;
var no_of_row = 0;
var no_of_digit = 0;
var time_interval = 0;
var answering_time = 0;
var timer = 0;
var question_type = 0;
var sliding_index = 0;
var delay = 0;
var level = 0;
var mini = [];
var maxi = [];
var diff = [];
var defa = [];
var sessionID = "";
var msg = [];

$("#toggleBack").click(function() {
    if(sliding_index == 2){
        back(2);
    }else if(sliding_index == 1){
        back(1);
    }else{
        window.history.back();
    }
});

$(window).unload(function() {
     var currentURL = window.location.href;
     var index = currentURL.indexOf("?");
     if(index > -1) {
        var tempURL = currentURL.substring(0, index);
        l(tempURL);
        window.location.replace(tempURL);
     }
});


/*document.onreadystatechange = function() { 
    if (document.readyState !== "complete") { 
        $(".loading")[0].style.display = "block";
        $(".home")[0].style.display = "none";
    } else { 
        $(".loading")[0].style.display = "none";
        $(".home")[0].style.display = "block";
    } 
};*/

window.onload = function () {
    if (typeof history.pushState === "function") {
        history.pushState("jibberish", null, null);
        window.onpopstate = function () {
            history.pushState('newjibberish', null, null);
            if(sliding_index == 2){
                back(2);
            }else if(sliding_index == 1){
                back(1);
            }else{
                window.history.back();
            }
        };
    }
    else {
        var ignoreHashChange = true;
        window.onhashchange = function () {
            if (!ignoreHashChange) {
                ignoreHashChange = true;
                window.location.hash = Math.random();
            }
            else {
                ignoreHashChange = false;   
            }
        };
    }

    for (var key in localStorage){
        if(key.includes("sma_")){
            sessionID = key;
            break;
        }
    }

    const now = new Date();
    const rand = pad(parseInt(Math.random() * 10000),5);

    if (window.location.search.substr(1) == "p"){
        if(sessionID != ""){
            const nowTime = new Date();
            if(localStorage.getItem(sessionID) > nowTime.getTime()){
                $(".home")[0].style.display = "none";
                $(".header")[0].style.display = "block";
                $(".content")[0].style.display = "block";

                sliding_index = 1;
            }else{
                localStorage.removeItem(sessionID);
                localStorage.setItem("sma_" + rand, now.getTime() + 3600000);
            }
        }else{
            localStorage.setItem("sma_" + rand, now.getTime() + 3600000);
        }     
    }else{
        localStorage.removeItem(sessionID);
        localStorage.setItem("sma_" + rand, now.getTime() + 3600000);
    }

    
    
};

$(".startHome").click(function(){
    $(".home")[0].style.display = "none";
    $(".header")[0].style.display = "block";
    $(".content")[0].style.display = "block";

    sliding_index = 1;
});

$( document ).ready(function() {
    loadJSON(function(response) {
      // Parse JSON string into object
        var data_json = JSON.parse(response);
    	var j = 12;
        for(var i = 0; i < $(".levelBtn").length; i++){
        	
        	$(".levelBtn")[i].style.backgroundColor = data_json.LevelColor[i];
        	$(".level_number")[i].style.color = data_json.LevelColor[i];	
        	$(".level_number")[i].innerHTML = j--;
        }
      
     });
});

function showToolTip(ele) {
    var ele = "#" + ele.id + " span";
    $(ele).addClass("show");
};

function hideToolTip(ele) {
    var ele = "#" + ele.id + " span";
    $(ele).removeClass("show");
};

function customPage(index) {

    if(index >= 3 && index <= 11)
    {
        msg = [];
        loadJSON(function(response) {
            // Parse JSON string into object
            var data_json = JSON.parse(response);
            var type_text = ["Addition","Addition/Subtraction","Multiplication","Division"];
            level = index + 1;
            $(".custom_value")[0].value = data_json.Levels["L"+(index+1)].question;
            $(".custom_value")[1].value = data_json.Levels["L"+(index+1)].number;
            //$(".custom_value")[2].value = parseValue(data_json.Levels["L"+(index+1)].digit, "n2d");
            $("#digit_sel")[0].value = data_json.Levels["L"+(index+1)].digit;
            $(".custom_value")[2].value = data_json.Levels["L"+(index+1)].timeInterval;
            $(".custom_value")[3].value = data_json.Levels["L"+(index+1)].completeTime;

            if(parseInt(data_json.Levels["L"+(index+1)].answerTime) == 5000)
                $("#fiveS")[0].checked = true;
            else
                $("#tenS")[0].checked = true;
            
            $("#quesType")[0].innerHTML = "";

            for(var i = 0; i < 4; i++){    
                if(data_json.Levels["L"+(index+1)].types[i] == 1){
                    var str = '<option value="' + (parseInt(i)+1) + '">' + type_text[i] + '</option>';
                    $("#quesType").append(str);
                }
            }

            msg.push(data_json.Levels["L"+(index+1)].notes[0]);
            msg.push(data_json.Levels["L"+(index+1)].notes[1]);
            
            insertVariable();

            if(question_type >= 3){
                $(".custom_value")[1].disabled = true;
                $(".left")[2].style.display = "none";
                $(".right")[2].style.display = "none";
                $(".custom_value")[1].value = 2;
                $(".custom_value")[2].value = 10000;

                $("#digit_sel")[0].disabled = true;
                $(".left")[3].style.display = "none";
                $(".right")[3].style.display = "none";

                $("#notes")[0].style.display = "inline";
                if(question_type == 3){
                    $("#notes")[0].innerHTML = msg[0];
                }else{
                    $("#notes")[0].innerHTML = msg[1];
                }
            }else{
                $(".custom_value")[1].disabled = false;
                $(".left")[2].style.display = "";
                $(".right")[2].style.display = "";
                $(".custom_value")[2].value = 1000;

                $("#digit_sel")[0].disabled = false;
                $(".left")[3].style.display = "";
                $(".right")[3].style.display = "";

                $("#notes")[0].style.display = "";
                $("#notes")[0].innerHTML = "";
            }

            delay = parseInt(data_json.Rules.delay);
            
            mini[0] = parseInt(data_json.Rules["CustomQuestion"].minimum);
            mini[1] = parseInt(data_json.Rules["CustomRow"].minimum);
            mini[2] = parseInt(data_json.Rules["CustomTimeInterval"].minimum);

            maxi[0] = parseInt(data_json.Rules["CustomQuestion"].maximum);
            maxi[1] = parseInt(data_json.Rules["CustomRow"].maximum);
            maxi[2] = parseInt(data_json.Rules["CustomTimeInterval"].maximum);
        

            diff[0] = parseInt(data_json.Rules["CustomQuestion"].difference);
            diff[1] = parseInt(data_json.Rules["CustomRow"].difference);
            diff[2] = parseInt(data_json.Rules["CustomTimeInterval"].difference);

            defa[0] = parseInt(data_json.Levels["L"+(index+1)].question);
            defa[1] = parseInt(data_json.Levels["L"+(index+1)].number);
            defa[2] = parseInt(data_json.Levels["L"+(index+1)].timeInterval);

        });

        $(".background")[0].style.display = "block";
        $(".content h2")[0].innerHTML = "Custom Make Page";
        $(".grid_container")[0].style.display = "none";
        $(".custom")[0].style.display = "grid";
        
    }
    else
    {
        alert("This level is not available yet.")
    }
    
    sliding_index = 2;
};



function insertVariable() {
    no_of_question = $(".custom_value")[0].value;
    no_of_row = $(".custom_value")[1].value;
    no_of_digit = $("#digit_sel")[0].value;
    time_interval = $(".custom_value")[2].value;
    timer = $(".custom_value")[3].value;

    if($("#fiveS")[0].checked)
        answering_time = $("#fiveS")[0].value;
    else 
        answering_time = $("#tenS")[0].value;

    question_type = $("#quesType").val();

    //l($("#add")[0].checked);
    //l($("#add")[0].checked && $("#sub")[0].checked);
    //l(no_of_question);
    //l(no_of_row);
    //l(no_of_digit);
    //l($(".custom_value")[2].value);
    //l(answering_time);
    //l("Timer:" + timer);
    //l(question_type);

};

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (event.charCode >= 47 && event.charCode <= 57)
    {
        return true;
    }
    else
        return false;
};

function validateRange(index) 
{
    //l(no_of_question);
    var ele = $(".custom_value")[index];

    /*var min;
    var max;
    var diff;
    var def;
    
    if(index == 0)
    {
        min = 1;
        max = 30;
        diff = 1;
        def = no_of_question;
    }
    else if(index == 1)
    {
        min = 2;
        max = 15;
        diff = 1;
        def = no_of_row;
    }
    else if(index == 2)
    {
        min = 1;
        max = 5;
        diff = 0.5;
        def = no_of_digit;
    }
    else
    {
        min = 100;
        max = 3000;
        diff = 50;
        def = time_interval;
    }*/

    if(isNaN($(".custom_value")[index].value)  || $(".custom_value")[index].value == "")
        ele.value = defa[index];
    else if(parseInt(ele.value) < mini[index])
        ele.value = mini[index];
    else if(ele.value > maxi[index])
        ele.value = maxi[index]; 
    else 
        ele.value = ele.value;

    if(index == 3)
    {
        if(parseInt(ele.value) % diff[index] != 0)
            ele.value = time_interval;
    }

    cal();
};


$('input[name=quesType]').change(function(){
    var checked = false;
    if($(".ques_type")[1].checked)
        $(".ques_type")[0].checked = true;

    for (var i = 0; i < 2; i++) {
        if ($(".ques_type")[i].checked)
            checked = true;   
    }

    if(!checked)
        $(".ques_type")[0].checked = true;
});

function cal() {
    //l($(".custom_value")[3].value);
    no_of_question = $(".custom_value")[0].value;
    no_of_row = $(".custom_value")[1].value;
    time_interval = $(".custom_value")[2].value;
    if($("#fiveS")[0].checked)
        answering_time = $("#fiveS")[0].value;
    else 
        answering_time = $("#tenS")[0].value;
    
    if($("#quesType")[0].value >= 3){
        $(".custom_value")[3].value = parseInt(no_of_question*time_interval) + parseInt(no_of_question*answering_time);
    }else{
        $(".custom_value")[3].value = parseInt(no_of_question*no_of_row*time_interval) + parseInt(no_of_question*answering_time);
    }
    
};

function ope(index, operation) {
    //var min;
    //var max;
    //var diff;
    var ele = $(".custom_value")[index];

    no_of_question = $(".custom_value")[0].value;
    no_of_row = $(".custom_value")[1].value;
    time_interval = $(".custom_value")[2].value;
    timer = $(".custom_value")[3].value;


    if(operation == 0)
    {
        if(parseInt(ele.value) > mini[index])
        {
            ele.value = parseInt(ele.value) - diff[index];
       }
    }
    else
    {
        if(parseInt(ele.value) < maxi[index])
        {
            ele.value = parseInt(ele.value) + diff[index];
        }
    }

    cal();
};

function parseValue(value, index){
    value = value.toString();
    if(index == "n2d"){ //numver to display
        if(value.includes(".")){
            value = Math.floor(value) + "/" + (Math.floor(value) + 1);
            return value;
        }else{
            return value;
        }
    }else{ // dipslay to number
        if(value.includes("/")){
            value = value[0];
            value = Math.floor(value) + ".5";
            return parseFloat(value);
        }else{
            return parseInt(value);
        }
    }
}

$("#quesType").change((e)=>{
    if(e.target.value >= 3){
        $(".custom_value")[1].disabled = true;
        $(".left")[2].style.display = "none";
        $(".right")[2].style.display = "none";
        $(".custom_value")[1].value = 2;
        $(".custom_value")[2].value = 10000;

        $("#digit_sel")[0].disabled = true;
        $(".left")[3].style.display = "none";
        $(".right")[3].style.display = "none";

        $("#notes")[0].style.display = "inline";
        if(e.target.value == 3){
            $("#notes")[0].innerHTML = msg[0];
        }else{
            $("#notes")[0].innerHTML = msg[1];
        }
    }else{
        $(".custom_value")[1].disabled = false;
        $(".left")[2].style.display = "";
        $(".right")[2].style.display = "";
        $(".custom_value")[2].value = 1000;

        $("#digit_sel")[0].disabled = false;
        $(".left")[3].style.display = "";
        $(".right")[3].style.display = "";

        $("#notes")[0].style.display = "";
        $("#notes")[0].innerHTML = "";
    }
    cal();
});

function addDelay() {
    timer = parseInt(timer) + (parseInt(delay) * (parseInt(no_of_question) - 1));
    //l(timer);
}

$(".startGame").click(function(e){
    //$(".loading").fadeIn("fast");
    if(no_of_question != 0 && no_of_row != 0 && no_of_digit != "" && time_interval != 0 && answering_time != 0 && timer != 0  && question_type != 0)
    {
        $(".ctnr")[0].style.display = "flex";
        e.preventDefault();
        insertVariable();
        addDelay();

        $.ajax({
            type: "POST",
            url: "res/createRoom.php",
            dataType: "json",
            data: {
                no_of_question: no_of_question,
                no_of_row: no_of_row,
                no_of_digit: no_of_digit,
                time_interval: time_interval,
                answering_time: answering_time,
                timer: (timer/1000),
                question_type: question_type,
                delay: delay,
                level: level
            },
            success : function(data){
                if(data.status)
                    location.href = "room.php";
            }
        });
    }
});

function back(index){
    if(index == 1){
        $(".header")[0].style.display = "none";
        $(".content")[0].style.display = "none";
        $(".home")[0].style.display = "block";
        sliding_index = 0;

    }else{
        $(".background")[0].style.display = "none";
        $(".content h2")[0].innerHTML = "Choose Level";
        $(".grid_container")[0].style.display = "grid";
        $(".custom")[0].style.display = "none";
        sliding_index = 1;
    }
};
