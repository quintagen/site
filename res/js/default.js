var l = console.log;
var jsonversion = "data.json";

function loadJSON(callback) {   
    var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
    xobj.open('GET', "res/js/" + jsonversion, true);
    xobj.onreadystatechange = function () {
          if (xobj.readyState == 4 && xobj.status == "200") {
            callback(xobj.responseText);
          }
    };
    xobj.send(null);  
}

$("#toggleVolume").click(function() {
    $("#toggleVolume").toggleClass("fa-volume-up");
    $("#toggleVolume").toggleClass("fa-volume-mute");
});