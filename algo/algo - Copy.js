class Room{
	constructor(qNo, num, digit, option){
		this.question = Array(qNo);
		this.option = option;
		this.aLength = num * 2 - 1;
		this.digitNum = digit;
		this.startingValue = 0;
		this.digit = [];
		if(digit == 1){
			this.digit.push(9);
		}else if(digit == 1.5){
			this.digit.push(9);
			this.digit.push(99);
		}else if(digit == 2){
			this.digit.push(99);
			this.startingValue = 10;
		}else if(digit == 2.5){
			this.digit.push(99);
			this.digit.push(999);
			this.startingValue = 10;
		}else if(digit == 3){
			this.digit.push(999);
			this.startingValue = 100;
		}else if(digit == 3.5){
			this.digit.push(999);
			this.digit.push(9999);
			this.startingValue = 100;
		}else if(digit == 4){
			this.digit.push(9999);
			this.startingValue = 1000;
		}else if(digit == 4.5){
			this.digit.push(9999);
			this.digit.push(99999);
			this.startingValue = 1000;
		}else if(digit == 5){
			this.digit.push(99999);
			this.startingValue = 10000;
		}

		for(var i = 0; i < qNo; i++){
			this.question[i] = new Array(this.aLength);
			for(var j = 0; j < this.aLength; j++){
				this.question[i][j] = 0;
			}
		}

		for(var i = 0; i < qNo; i++){
			switch(this.option){

				case 1: 
					for (var j = 0; j < this.aLength; j += 2) {
						//l(this.digit);
						if(this.digit.length > 1){
							var randDigit = Math.floor(Math.random() * 100);
							if(randDigit > 50){
								randDigit = 0;
							}else{
								randDigit = 1;
							}
						}else{
							randDigit = 0;
						}

						do{
							this.question[i][j] = Math.ceil(Math.random() * this.digit[randDigit]);
						}while(this.question[i][j] < this.startingValue)

						if(j < this.aLength - 1){
							this.question[i][j+1] = 1;
						}
					}
					break;
				case 2:
					var isNegative = false;
					var numBefore = 0;
					
					for (var j = 0; j < this.aLength; j+=2) {
						//l(this.digit);
						if(this.digit.length > 1){
							var randDigit = Math.floor(Math.random() * 100);
							if(randDigit > 50){
								randDigit = 0;
							}else{
								randDigit = 1;
							}
						}
						else{
							randDigit = 0;
						}

						//l(this.digit.length);
						do{
							this.question[i][j] = Math.ceil(Math.random() * this.digit[randDigit]);
						}while(this.question[i][j] < this.startingValue)
						
						if(isNegative){
							var rand = Math.ceil(Math.random() * 100);
							var valid = true;
							do{
								this.question[i][j] = Math.ceil(Math.random() * Math.abs(this.answer(i)));
								
								/*if(this.question[i][j] == this.answer(i)){
									valid = false;
								}else if(this.question[i][j] == numBefore){
									valid = false;
								}else{
									valid = true;
								}*/
							}while(this.question[i][j] >= this.answer(i) || this.question[i][j] == 0 || this.question[i][j] < this.startingValue);
						}
						else{
							do{
								this.question[i][j] = Math.ceil(Math.random() * this.digit[randDigit]);
							}while(this.question[i][j] < this.startingValue)
							
						}
						//console.log(this.question[i]);
						//console.log(this.answer(i));
						if(j < this.aLength - 1){
							if(this.answer(i) > this.digit[randDigit] * 0.3)
								this.question[i][j+1] = Math.ceil(Math.random() * 2);
							else
								this.question[i][j+1] = 1;
							/*if(isNegative){
								console.log("rand: " + rand);
								if(this.digit > 9 && this.answer(i) < this.answer(i) * 0.2){
									this.question[i][j+1] = 1;
								}else if(rand < 85){
									this.question[i][j+1] = 1;
								}else{
									this.question[i][j+1] = Math.ceil(Math.random() * 2);
								}
							}
							else{
								this.question[i][j+1] = Math.ceil(Math.random() * 2);
								if(j = 0){
									numBefore = this.question[i][j];
								}
							}*/

							if(this.question[i][j+1] == 2){
								isNegative = true;
							}else{
								isNegative = false;
							}
							//console.log(isNegative);
						}
					}
					break;
				case 3:

			}
		};

		/*
		number = number of number
		operator:
			1 = only addition
			2 = addition + subtraction
		*/

	};

	getQuestionArray(){
		var data = [];
		for(var no = 0; no < this.question.length; no++){
			var tempQues = [];
			for (var i = 0; i < this.aLength; i+=2) {
				var tempNum = this.question[no][i];
				if(i == 0){
					tempQues.push(tempNum);
				}else{
					if(this.question[no][i-1] != 1){
						tempQues.push(this.getOperator(this.question[no][i-1]) + tempNum);
					}else{
						tempQues.push(tempNum);
					}
				}
					
			}
			data.push(tempQues);
		}
		return data;
	};


	getOperator(operator){
		if(operator == 1){
			return "+";
		}else if(operator == 2){
			return "-";
		}else if(operator == 3){
			return "*";
		}else if(operator == 4){
			return "/";
		}
	};

	show(no){
		var ques = "";
		for (var i = 0; i < this.aLength; i+=2) {
			ques += this.question[no][i];
			if(i < this.aLength - 1){
				ques += " " + this.getOperator(this.question[no][i+1]) + " ";
			}
		}

		return ques;
	};

	answer(no){
		var ans = this.question[no][0];
		for (var i = 0; i < this.aLength; i+=2) {
			if(i < this.aLength - 1){
				ans = this.cal(parseInt(ans), parseInt(this.question[no][i+1]), parseInt(this.question[no][i+2]));
			}
		}

		return ans;
	};

	cal(ans, ope, number){
		if(ope == 1){
			return ans + number;
		}else if(ope == 2){
			return ans - number;
		}else if(ope == 3){
			return ans * number;
		}else if(ope == 4){
			return ans / number;
		}else{
			return ans;
		}
	};
};

function pad(n, width, z) {
	var z = z || '0';
	var n = n + '';
	return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};