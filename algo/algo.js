class Room{
	constructor(qNo, num, digit, option, level=0){
		this.question = Array(qNo);
		this.option = option;
		this.aLength = num * 2 - 1;
		this.digitNum = digit.toString();
		this.startingValue = 0;
		this.level = level;
		this.decimal = false;

		if(level <= 8 && option >= 3){

			if(option == 3){
				switch(this.level){
					case 8:{
						this.variety = 1;
						this.varieties = Array(this.variety);

						this.varieties[0] = new Array(4);
						this.varieties[0][0] = 999;
						this.varieties[0][1] = 100;
						this.varieties[0][2] = 9;
						this.varieties[0][3] = 1;
						break;
					}
					case 7:{
						this.variety = 1;
						this.varieties = Array(this.variety);

						this.varieties[0] = new Array(4);
						this.varieties[0][0] = 99;
						this.varieties[0][1] = 10;
						this.varieties[0][2] = 99;
						this.varieties[0][3] = 10;
						break;
					}
					case 6:{
						this.variety = 2;
						this.varieties = Array(this.variety);

						this.varieties[0] = new Array(4);
						this.varieties[0][0] = 999;
						this.varieties[0][1] = 100;
						this.varieties[0][2] = 99;
						this.varieties[0][3] = 10;

						this.varieties[1] = new Array(4);
						this.varieties[1][0] = 99;
						this.varieties[1][1] = 10;
						this.varieties[1][2] = 999;
						this.varieties[1][3] = 100;
						break;
					}
					case 5:{
						this.variety = 3;
						this.varieties = Array(this.variety);

						this.varieties[0] = new Array(4);
						this.varieties[0][0] = 999;
						this.varieties[0][1] = 100;
						this.varieties[0][2] = 999;
						this.varieties[0][3] = 100;

						this.varieties[1] = new Array(4);
						this.varieties[1][0] = 9999;
						this.varieties[1][1] = 1000;
						this.varieties[1][2] = 99;
						this.varieties[1][3] = 10;

						this.varieties[2] = new Array(4);
						this.varieties[2][0] = 99;
						this.varieties[2][1] = 10;
						this.varieties[2][2] = 9999;
						this.varieties[2][3] = 1000;
						break;
					}
					case 4:{
						this.variety = 4;
						this.varieties = Array(this.variety);

						this.varieties[0] = new Array(4);
						this.varieties[0][0] = 9999;
						this.varieties[0][1] = 1000;
						this.varieties[0][2] = 999;
						this.varieties[0][3] = 100;

						this.varieties[1] = new Array(4);
						this.varieties[1][0] = 999;
						this.varieties[1][1] = 100;
						this.varieties[1][2] = 9999;
						this.varieties[1][3] = 1000;

						this.varieties[2] = new Array(4);
						this.varieties[2][0] = 99999;
						this.varieties[2][1] = 10000;
						this.varieties[2][2] = 99;
						this.varieties[2][3] = 10;

						this.varieties[3] = new Array(4);
						this.varieties[3][0] = 99;
						this.varieties[3][1] = 10;
						this.varieties[3][2] = 99999;
						this.varieties[3][3] = 10000;
						break;
					}
				}
			}else{
				switch(this.level){
					case 8:{
						this.variety = 1;
						this.varieties = Array(this.variety);

						this.varieties[0] = new Array(4);
						this.varieties[0][0] = 9;
						this.varieties[0][1] = 1;
						this.varieties[0][2] = 111;
						this.varieties[0][3] = 100;
						break;
					}
					case 7:{
						this.variety = 1;
						this.varieties = Array(this.variety);

						this.varieties[0] = new Array(4);
						this.varieties[0][0] = 9;
						this.varieties[0][1] = 1;
						this.varieties[0][2] = 1111;
						this.varieties[0][3] = 1000;
						break;
					}
					case 6:{
						this.variety = 2;
						this.varieties = Array(this.variety);

						this.varieties[0] = new Array(4);
						this.varieties[0][0] = 99;
						this.varieties[0][1] = 10;
						this.varieties[0][2] = 101;
						this.varieties[0][3] = 1000;

						this.varieties[1] = new Array(4);
						this.varieties[1][0] = 99;
						this.varieties[1][1] = 10;
						this.varieties[1][2] = 10;
						this.varieties[1][3] = 100;
						break;
					}
					case 5:{
						this.variety = 2;
						this.varieties = Array(this.variety);

						//12345 ÷ 12
						this.varieties[0] = new Array(4);
						this.varieties[0][0] = 99;
						this.varieties[0][1] = 10;
						this.varieties[0][2] = 1010;
						this.varieties[0][3] = 10000;

						//12345 ÷ 123
						this.varieties[1] = new Array(4);
						this.varieties[1][0] = 999;
						this.varieties[1][1] = 100;
						this.varieties[1][2] = 100;
						this.varieties[1][3] = 10000;
						break;
					}
					case 4:{
						this.variety = 6;
						this.varieties = Array(this.variety);

						//12345 ÷ 12
						this.varieties[0] = new Array(4);
						this.varieties[0][0] = 99;
						this.varieties[0][1] = 10;
						this.varieties[0][2] = 1010;
						this.varieties[0][3] = 10000;

						//12345 ÷ 123
						this.varieties[1] = new Array(4);
						this.varieties[1][0] = 999;
						this.varieties[1][1] = 100;
						this.varieties[1][2] = 100;
						this.varieties[1][3] = 10000;

						//12345 ÷ 1234
						this.varieties[2] = new Array(4);
						this.varieties[2][0] = 9999;
						this.varieties[2][1] = 1000;
						this.varieties[2][2] = 10;
						this.varieties[2][3] = 10000;

						//123456 ÷ 12
						this.varieties[3] = new Array(4);
						this.varieties[3][0] = 99;
						this.varieties[3][1] = 10;
						this.varieties[3][2] = 10101;
						this.varieties[3][3] = 100000;

						//123456 ÷ 123
						this.varieties[4] = new Array(4);
						this.varieties[4][0] = 999;
						this.varieties[4][1] = 100;
						this.varieties[4][2] = 1001;
						this.varieties[4][3] = 100000;

						//123456 ÷ 1234
						this.varieties[5] = new Array(4);
						this.varieties[5][0] = 9999;
						this.varieties[5][1] = 1000;
						this.varieties[5][2] = 100;
						this.varieties[5][3] = 100000;

						break;
					}
				}
			}
			
		}else{
			if(this.digitNum.indexOf(",") == -1){
				this.digit = [];
				if(digit == 1){
					this.digit.push(9);
				}else if(digit == 1.5){
					this.digit.push(9);
					this.digit.push(99);
				}else if(digit == 2){
					this.digit.push(99);
					this.startingValue = 10;
				}else if(digit == 2.5){
					this.digit.push(99);
					this.digit.push(999);
					this.startingValue = 10;
				}else if(digit == 3){
					this.digit.push(999);
					this.startingValue = 100;
				}else if(digit == 3.5){
					this.digit.push(999);
					this.digit.push(9999);
					this.startingValue = 100;
				}else if(digit == 4){
					this.digit.push(9999);
					this.startingValue = 1000;
				}else if(digit == 4.5){
					this.digit.push(9999);
					this.digit.push(99999);
					this.startingValue = 1000;
				}else if(digit == 5){
					this.digit.push(99999);
					this.startingValue = 10000;
				}
			}else{
				this.digit = [];
				this.decimal = true;
				this.digitNum = this.digitNum.slice(0, -1);
				switch(this.digitNum){
					case "1":{
						this.digit.push(99);
						this.digit.push(999);
						this.digit.push(9999);
						this.startingValue = 0;
						this.maxValue = 100;
						break;
					}
					case "1.5":{
						this.digit.push(999);
						this.digit.push(9999);
						this.startingValue = 1;
						this.maxValue = 100;
						break;
					}
					case "2":{
						this.digit.push(999);
						this.digit.push(9999);
						this.digit.push(99999);
						this.startingValue = 1;
						this.maxValue = 1000;
						break;
					}
				}
			}
				
		}
		

		for(var i = 0; i < qNo; i++){
			this.question[i] = new Array(this.aLength);
			for(var j = 0; j < this.aLength; j++){
				this.question[i][j] = 0;
			}
		}

		for(var i = 0; i < qNo; i++){
			switch(this.option){
				case 1: 
					for (var j = 0; j < this.aLength; j += 2) {
						//l(this.digit);
						var randDigit = Math.floor(Math.random() * 100) % this.digit.length;

						if(this.decimal == true){
							do{
								this.question[i][j] = Math.ceil(Math.random() * this.digit[randDigit]);
								this.question[i][j] = (this.question[i][j] / 100).toFixed(2);
							}while(this.question[i][j] < this.startingValue);
						}else{
							do{
								this.question[i][j] = Math.ceil(Math.random() * this.digit[randDigit]);
							}while(this.question[i][j] < this.startingValue);
						}
						

						if(j < this.aLength - 1){
							this.question[i][j+1] = 1;
						}
					}
					break;
				case 2:
					var isNegative = false;
					var numBefore = 0;
					var abs_positive = 0;
					var abs_negative = 0;
					var number_count = 1;
					
					for (var j = 0; j < this.aLength; j+=2) {
						//l(this.digit);

						var randDigit = Math.floor(Math.random() * 100) % this.digit.length;
						
						if(isNegative){
							if(this.decimal == true){
								var rand = Math.ceil(Math.random() * 100);
								do{
									this.question[i][j] = Math.ceil(Math.random() * Math.abs(this.answer(i)*100));
									this.question[i][j] = (this.question[i][j] / 100).toFixed(2);
								}while(this.question[i][j] >= this.answer(i) || this.question[i][j] == 0 || this.question[i][j] < this.startingValue || this.question[i][j] > this.maxValue);
							}else{
								var rand = Math.ceil(Math.random() * 100);
								var valid = true;
								do{
									this.question[i][j] = Math.ceil(Math.random() * Math.abs(this.answer(i)));
									
									/*if(this.question[i][j] == this.answer(i)){
										valid = false;
									}else if(this.question[i][j] == numBefore){
										valid = false;
									}else{
										valid = true;
									}*/
									l(this.answer(i));
								}while(this.question[i][j] >= this.answer(i) || this.question[i][j] == 0 || this.question[i][j] < this.startingValue);
							}
							
						}
						else{
	
							if(this.decimal == true){
								do{
									this.question[i][j] = Math.ceil(Math.random() * this.digit[randDigit]);
									this.question[i][j] = (this.question[i][j] / 100).toFixed(2);
								}while(this.question[i][j] < this.startingValue || this.question[i][j] > this.maxValue);
							}else{
								do{
									this.question[i][j] = Math.ceil(Math.random() * this.digit[randDigit]);
								}while(this.question[i][j] < this.startingValue);
							}
						}
						//console.log(this.question[i]);
						//console.log(this.answer(i));

						if(j < this.aLength - 1){

							if(this.answer(i) > this.digit[randDigit] * 0.4 && this.decimal == true && abs_positive < 2){
								this.question[i][j+1] = 2;
							}else if(this.answer(i) > this.digit[randDigit] * 0.3 && abs_positive < 2 || abs_negative >= 2){
								this.question[i][j+1] = Math.ceil(Math.random() * 2);
							}else{
								this.question[i][j+1] = 1;
							/*if(isNegative){
								console.log("rand: " + rand);
								if(this.digit > 9 && this.answer(i) < this.answer(i) * 0.2){
									this.question[i][j+1] = 1;
								}else if(rand < 85){
									this.question[i][j+1] = 1;
								}else{
									this.question[i][j+1] = Math.ceil(Math.random() * 2);
								}
							}
							else{
								this.question[i][j+1] = Math.ceil(Math.random() * 2);
								if(j = 0){
									numBefore = this.question[i][j];
								}
							}*/
							}

							if(this.question[i][j+1] == 2){
								isNegative = true;
								abs_negative = 0;
								abs_positive++;
							}else{
								abs_negative++;
								isNegative = false;
								abs_positive = 0;
							}
							number_count++;
							//console.log(isNegative);
						}
					}
					break;
				case 3:
					for(var i = 0; i < qNo; i++){

						var rand_varity = Math.ceil(Math.random() * this.variety) - 1;
						var digit_count = 0;

						for (var j = 0; j < this.aLength; j+=2) {
							do{
								this.question[i][j] = Math.ceil(Math.random() * this.varieties[rand_varity][digit_count]);
							}while(this.question[i][j] < this.varieties[rand_varity][digit_count+1]);
							digit_count += 2;
							this.question[i][1] = this.option;
						}
					}
					break;
				case 4:
					for(var i = 0; i < qNo; i++){

						var rand_varity = Math.ceil(Math.random() * this.variety) - 1;
						var digit_count = 0;
						var divisor = 0;

						do{
							this.question[i][2] = Math.ceil(Math.random() * this.varieties[rand_varity][digit_count]);
						}while(this.question[i][2] < this.varieties[rand_varity][digit_count+1]);
						digit_count += 2;
						this.question[i][1] = this.option;
						do{
							divisor = Math.ceil(Math.random() * this.varieties[rand_varity][digit_count]);
							this.question[i][0] = divisor * this.question[i][2];
						}while(this.question[i][0] > this.varieties[rand_varity][digit_count+1]*10 || this.question[i][0] < this.varieties[rand_varity][digit_count+1]);
					}
					break;

			}
			//l(this.question[i]);
		};
	};

	getDecimalString(number){
	    var input = parseFloat(number);
	    if(input < 1){
			var str = input.toFixed(2).toString();
	        str = str.substring(1);
		}else{
			var str = input.toFixed(2).toString();
		}
		return str;
	};

	getQuestionArray(){
		var data = "";
		data = [];
		for(var no = 0; no < this.question.length; no++){
			var tempQues = [];
			var quesShow = "";
			for (var i = 0; i < this.aLength; i+=2) {
				var tempNum = this.question[no][i];
				if(i == 0){
					if(this.decimal == true){
						tempQues.push(this.getDecimalString(tempNum));
					}else if(this.question[no][1] == 3){
						quesShow +=  tempNum;
					}else if(this.question[no][1] == 4){
						quesShow += tempNum;
					}else{
						tempQues.push(tempNum);
					}
					
				}else{
					if(this.question[no][i-1] == 2){
						if(this.decimal == true){
							tempQues.push(this.getOperator(this.question[no][i-1]) + " " + this.getDecimalString(tempNum));
						}else{
							tempQues.push(this.getOperator(this.question[no][i-1]) + tempNum);
						}
						
					}else if(this.question[no][i-1] == 3){
						quesShow += "<br>×<br>" + tempNum;
						tempQues.push(quesShow);
					}else if(this.question[no][i-1] == 4){
						quesShow += "<br>÷<br>" + tempNum;
						tempQues.push(quesShow);
					}else{
						if(this.decimal == true){
							tempQues.push(this.getDecimalString(tempNum));
						}else{
							tempQues.push(tempNum);
						}
					}
				}

					
			};
			data.push(tempQues);
		};
		return data;
	};

	
	getOperator(operator){
		if(operator == 1){
			return "+";
		}else if(operator == 2){
			return "-";
		}else if(operator == 3){
			return "*";
		}else if(operator == 4){
			return "/";
		}
	};

	show(no){
		var ques = "";
		for (var i = 0; i < this.aLength; i+=2) {
			ques += this.question[no][i];
			if(i < this.aLength - 1){
				ques += " " + this.getOperator(this.question[no][i+1]) + " ";
			}
		}

		return ques;
	};

	answer(no){
		var ans = this.question[no][0];
		for (var i = 0; i < this.aLength; i+=2) {
			if(i < this.aLength - 1){
				if(this.decimal == true){
					ans = parseFloat(this.cal(parseFloat(ans).toFixed(2), parseInt(this.question[no][i+1]), parseFloat(this.question[no][i+2]).toFixed(2))).toFixed(2);
				}else{
					ans = this.cal(parseInt(ans), parseInt(this.question[no][i+1]), parseInt(this.question[no][i+2]));
				}
				
			}
		}

		return ans;
	};

	cal(ans, ope, number){
		
		if(ope == 1){
			if(this.decimal == true){
				ans = parseFloat(ans);
				number = parseFloat(number);
				return parseFloat(ans + number).toFixed(2);
			}else{
				return ans + number;
			}
		}else if(ope == 2){
			if(this.decimal == true){
				ans = parseFloat(ans);
				number = parseFloat(number);
				return parseFloat(ans - number).toFixed(2);
			}else{
				return ans - number;
			}
		}else if(ope == 3){
			return ans * number;
		}else if(ope == 4){
			return ans / number;
		}else{
			return ans;
		}
	};
};

function pad(n, width, z) {
	var z = z || '0';
	var n = n + '';
	return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};