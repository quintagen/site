<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" sizes="16x16" href="res/images/SamLogo.png">
    <meta name="theme-color" content="#339933" />
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Goudy+Bookletter+1911|Roboto|Bubblegum Sans" />
    <link rel="stylesheet" type="text/css" href="res/css/choose.min.css?v1.4">
	<title>Sam Mental Arithmetic</title>
</head>
<body>
	<img src="res/images/SamLogo.png" hidden>
	<div class="container">
		<div class="blocker">
			<span class="helper"></span><img src="res/images/screen_rotation.png" class="rotate">
			<span class="rotate_reminder">Please rotate your screen</span>
		</div>
		<div class="loading">
			<img class="loading-logo" src="res/images/SamLogo.svg">
			<span class="loading-title">LOADING</span>
			<div class="dots">
				<span class="dot0">.</span>
				<span class="dot1">.</span>
				<span class="dot2">.</span>
				<span class="dot3">.</span>
				<span class="dot4">.</span>
				<span class="dot5">.</span>
			</div> 
		</div>
		<div class="home">
			<div class="circle a brown"><i class="fas fa-plus"></i></div>
			<div class="circle b brown"><i class="fas fa-minus"></i></div>
			<div class="circle c brown"><i class="fas fa-divide"></i></div>
			<div class="circle d brown"><i class="fas fa-times"></i></div>
			<div class="circle e orange"><i class="fas fa-divide"></i></div>
			<div class="circle f orange"><i class="fas fa-times"></i></div>
			<div class="circle g orange"><i class="fas fa-plus"></i></div>
			<div class="circle h orange"><i class="fas fa-minus"></i></div>
			<div class="circle i green"><i class="fas fa-plus"></i></div>
			<div class="circle j green"><i class="fas fa-minus"></i></div>
			<div class="circle k green"><i class="fas fa-divide"></i></div>
			<div class="circle l green"><i class="fas fa-times"></i></div>
			
			<div class="welcome">
				<span class="home-title home-orange">W</span><!--
				--><span class="home-title home-yellow">e</span><!--
				--><span class="home-title home-yellow">l</span><!--
				--><span class="home-title home-sea">c</span><!--
				--><span class="home-title home-white">o</span><!--
				--><span class="home-title home-purple">m</span><!--
				--><span class="home-title home-green">e</span>
			</div>
			<img class="logo" src="res/images/SamLogo.svg">
			<div class="sam-arithmetic">
				<span class="home-title home-yellow">S</span><!--
				--><span class="home-title home-yellow">a</span><!--
				--><span class="home-title home-yellow">m</span>&nbsp;
				<span class="home-title home-blue">M</span><!--
				--><span class="home-title home-white">e</span><!--
				--><span class="home-title home-purple">n</span><!--
				--><span class="home-title home-orange">t</span><!--
				--><span class="home-title home-green">a</span><!--
				--><span class="home-title home-green">l</span><br>
				<span class="home-title home-yellow">A</span><!--
				--><span class="home-title home-yellow">r</span><!--
				--><span class="home-title home-green">i</span><!--
				--><span class="home-title home-blue">t</span><!--
				--><span class="home-title home-blue">h</span><!--
				--><span class="home-title home-blue">m</span><!--
				--><span class="home-title home-purple">e</span><!--
				--><span class="home-title home-purple">t</span><!--
				--><span class="home-title home-orange">i</span><!--
				--><span class="home-title home-green">c</span>
			</div>
			<button class="startHome">Start</button>

		</div>

		<div class="header">
			<i id="toggleBack" class="fas fa-arrow-left"></i>
			<img src="res/images/SamLogo.svg">
			<i id="toggleVolume" class="fas fa-volume-up"></i>
		</div>
		<div class="content">
			<div class="background">
				<div class="circle a brown"></div>
				<div class="circle b brown"></div>
				<div class="circle c brown"></div>
				<div class="circle d brown"></div>
				<div class="circle e orange"></div>
				<div class="circle f orange"></div>
				<div class="circle g orange"></div>
				<div class="circle h orange"></div>
				<div class="circle i brown"></div>
				<div class="circle j orange"></div>
			</div>
			<h2>Choose Level</h2>

			<div class="grid_container">
				<div>
					<div class="levelBtn open" onclick="customPage(11)">Level
						<div class="level_number"></div>
					</div>
					<div class="levelBtn open" onclick="customPage(10)">Level
						<div class="level_number"></div>
					</div>
				</div>
				<div>
					<div class="levelBtn open" onclick="customPage(9)">Level
						<div class="level_number"></div>
					</div>
					<div class="levelBtn open" onclick="customPage(8)">Level
						<div class="level_number"></div>
					</div>
				</div>
				<div>
					<div class="levelBtn open" onclick="customPage(7)">Level
						<div class="level_number"></div>
					</div>
					<div class="levelBtn open" onclick="customPage(6)">Level
						<div class="level_number"></div>
					</div>
				</div>
				<div>
					<div class="levelBtn open" onclick="customPage(5)">Level
						<div class="level_number"></div>
					</div>
					<div class="levelBtn open" onclick="customPage(4)">Level
						<div class="level_number"></div>
					</div>
				</div>
				<div>
					<div class="levelBtn open" onclick="customPage(3)">Level
						<div class="level_number"></div>
					</div>
					<div class="levelBtn">Level
						<div class="coming_soon">Coming Soon</div>
						<div class="level_number"></div>
					</div>
				</div>
				<div>
					<div class="levelBtn">Level
						<div class="coming_soon">Coming Soon</div>
						<div class="level_number"></div>
					</div>
					<div class="levelBtn">Level
						<div class="coming_soon">Coming Soon</div>
						<div class="level_number"></div>
					</div>
				</div>
			</div>

			<div class="custom">
				<div class="grid_container custom-grid">
					<div class="custom_option left">
						Question Type:
						<i class="far fa-question-circle" id="helpSeven" ontouchstart="showToolTip(this)" ontouchend="hideToolTip(this)"><span class="tooltiptext">Operator included in one question, either addtion or addtion + subtraction</span></i>
					</div>
					<div class="custom_input custom-box right">
						<select id="quesType">
							<option value="1">Addition</option>
							<option value="2">Addition / Subtraction</option>
							<option value="3">Multiplication</option>
							<option value="4">Division</option>
						</select>
					</div>
					<div class="custom_option left">
						No. Of Question:
						<i class="far fa-question-circle" id="helpOne" ontouchstart="showToolTip(this)" ontouchend="hideToolTip(this)"><span class="tooltiptext">Total number of question</span></i>

					</div>
					<div class="custom_input right">
						<button class="btn_left" onclick="ope(0,0)">-</button><!--
			    		--><input type="number" class="custom_value" value="1" onchange="validateRange(0)" onkeypress="return isNumberKey(event)"><!--
			    		--><button class="btn_right" onclick="ope(0,1)">+</button>
					</div>

					<div class="custom_option left">
						No. Of Row:
						<i class="far fa-question-circle" id="helpTwo" ontouchstart="showToolTip(this)" ontouchend="hideToolTip(this)"><span class="tooltiptext">Number of numbers appears in one question</span></i>
					</div>
					<div class="custom_input right">
						<button class="btn_left" onclick="ope(1,0)">-</button><!--
			    		--><input type="number" class="custom_value" value="1" onchange="validateRange(1)" onkeypress="return isNumberKey(event)"><!--
			    		--><button class="btn_right" onclick="ope(1,1)">+</button>
					</div>

					<div class="custom_option left">
						No. Of Digit:
						<i class="far fa-question-circle" id="helpThree" ontouchstart="showToolTip(this)" ontouchend="hideToolTip(this)"><span class="tooltiptext">Number of digit of a number</span></i>
					</div>
					<div class="custom_input right">
						<select id="digit_sel">
							<optgroup label="Without Decimal Points">
								<option value="1">1 digit</option>
								<option value="1.5">1/2 digits</option>
								<option value="2">2 digits</option>
								<option value="2.5">2/3 digits</option>
								<option value="3">3 digits</option>
								<option value="3.5">3/4 digits</option>
								<option value="4">4 digits</option>
								<option value="4.5">4/5 digits</option>
								<option value="5">5 digits</option>
							</optgroup>
							<optgroup label="With Decimal points">
								<option value="1,">0-2 digits with 2dp</option>
								<option value="1.5,">1-2 digits with 2dp</option>
								<option value="2,">1-3 digits with 2dp</option>
							</optgroup>

						</select>
					</div>

					<div class="custom_option left">
						Time Interval (ms):
						<i class="far fa-question-circle" id="helpFour" ontouchstart="showToolTip(this)" ontouchend="hideToolTip(this)"><span class="tooltiptext">Time between the numbers/question appear</span></i>
					</div>
					<div class="custom_input right">
						<button class="btn_left" onclick="ope(2,0)">-</button><!--
			    		--><input type="number" class="custom_value" onchange="validateRange(2)" onkeypress="return isNumberKey(event)"><!--
			    		--><button class="btn_right" onclick="ope(2,1)">+</button>
					</div>


					<div class="custom_option left">
						Answering Time (ms):
						<i class="far fa-question-circle" id="helpFive" ontouchstart="showToolTip(this)" ontouchend="hideToolTip(this)"><span class="tooltiptext">Time set to answer one question</span></i>
					</div>
					<div class="custom_input custom-box right">
						<div class="inputOne">
							<input type="radio" id="fiveS" name="answerTime" value="5000" onchange="cal()">
							<label>5000</label>
						</div>
						<div class="inputTwo">
							<input type="radio" id="tenS" name="answerTime" value="10000" onchange="cal()" checked>
							<label>10000</label>
						</div>
					</div>
					<div class="custom_option left">
						Timer (ms):
						<i class="far fa-question-circle" id="helpSix" ontouchstart="showToolTip(this)" ontouchend="hideToolTip(this)"><span class="tooltiptext">Total time set to answer all question</span></i>
					</div>
					<div class="custom_input right">
			    		<input type="number" class="custom_value" value="0" disabled>
					</div>
				</div>
				<span class="note">Note: 1s = 1000ms</span>
				<span id="notes"></span>
				<button class="startGame">Start</button>
			</div>
		</div>
		<section class="ctnr">
			<div class="ldr">
				<div class="ldr-blk"></div>
				<div class="ldr-blk an_delay"></div>
				<div class="ldr-blk an_delay"></div>
				<div class="ldr-blk"></div>
			</div>
		</section>
	</div>
	<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript" src="https://www.quintagen.com/res/js/tools_v1.0.min.js"></script>
	<script type="text/javascript" src="res/js/default.min.js?v1.0"></script>
	<script type="text/javascript" src="res/js/loading.min.js?v1.0"></script>
	<script type="text/javascript" src="res/js/choose.min.js?v1.3"></script>

</body>
</html>